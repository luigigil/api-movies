package com.api.movies;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

/**
 * @author luigi
 *
 */
public class Main {
	
	public static final String BASE_URI = "http://localhost:9000/api/v1";
	
	public static HttpServer server() {
		final ResourceConfig rc = new ResourceConfig().packages("com.api.movies");
		
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
	}
		
	public static void main(String[] args) throws IOException {
		final HttpServer server = server();
		System.out.println("Aplicação ouvindo em http://localhost:9000/api/v1");
		System.out.println("Digite q para encerrar.");
		while(true) {
			int key = System.in.read();
			if(key == 113)
				server.shutdownNow();			
		}
	}
}