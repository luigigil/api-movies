package com.api.movies.models;

public class Movie {
	private long id;
	private String name;
	private int imdbScore;
	
	public Movie() {}
	
	public Movie(long id, String name, int imdbScore) {
		this.id = id;
		this.name = name;
		this.imdbScore = imdbScore;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getImdbScore() {
		return imdbScore;
	}

	public void setImdbScore(int imdbScore) {
		this.imdbScore= imdbScore;
	}
	
	@Override
	public String toString() {
		return "{" + "id=" + id + 
				", name='" + name + '\'' + 
				", imdbScore=" + imdbScore + 
				"}";		
	}
}