package com.api.movies.repositories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.api.movies.models.Movie;

public class MoviesRepository implements BaseRepository<Movie> {
	private static List<Movie> movies;

	public MoviesRepository() {
		movies = new ArrayList<>(Arrays.asList(
                new Movie(0, "movie1", 9),
                new Movie(1, "movie2", 8),
                new Movie(2, "movie3", 7)
        ));		
	}
	
	
	@Override
	public List<Movie> findAll() {
		return movies;
	}

	@Override
	public Movie findById(long id) {
		return movies.stream().filter(movie -> movie.getId() == id).findFirst().get();
	}

	@Override
	public void add(Movie entity) {
		entity.setId(movies.size());
		movies.add(entity);
		
	}

	@Override
	public void update(Movie entity) {
		Movie movie = this.findById(entity.getId());
		
		movie.setName(entity.getName());
		movie.setImdbScore(entity.getImdbScore());
	}

	@Override
	public void remove(long id) {
		movies = movies.stream().filter(movie -> movie.getId() != id).collect(Collectors.toList());
		
	}
}