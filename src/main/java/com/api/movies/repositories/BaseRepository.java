package com.api.movies.repositories;

import java.util.List;

public interface BaseRepository<T> {
	List<T> findAll();
	T findById(long id);
	void add(T entity);
	void update(T entity);
	void remove(long id);
}